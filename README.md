# Computação Concorrente

## Laboratório 4 - Raiz do primos em um vetor, sequencial e concorrente

<br>

### Michel Monteiro Schorr 120017379

<br><br>

### Dentro da pasta lab, irá encontrar 3 pastas:  
**execs**, contendo os executaveis  
**libs**, contendo as bibliotecas, macros, funcoes, estruturas, etc  
**src**, contendo o condigo fonte principal

<br><br>

## Comandos

Os comandos para compilar e executar são os seguintes:
<br>
<br>


``` 
./lab/src/primes_sqrt.c -o ./lab/execs/primes_sqrt -Wall -lpthread -lm
./lab/execs/primes_sqrt dim nthreads
```
<br><br><br><br>




# Resultados
<br>

# dim 10^5


## dim 10^5 ______threads 1

### _________sequencial x concorrente

### teste 1: 0.069211 __ x __ 0.062298
### teste 2: 0.057667 __ x __ 0.063144
### teste 3: 0.070171 __ x __ 0.062632
### teste 4: 0.030588 __ x __ 0.034448
### teste 5: 0.069914 __ x __ 0.064372





<br><br>




## dim 10^5 ______threads 2

### _________sequencial x concorrente

### teste 1: 0.069512 __ x __ 0.047493
### teste 2: 0.068179 __ x __ 0.049029
### teste 3: 0.066473 __ x __ 0.050622
### teste 4: 0.047985 __ x __ 0.052754
### teste 5: 0.069799 __ x __ 0.051798





<br><br>

## dim 10^5 ______threads 4

### _________sequencial x concorrente

### teste 1: 0.052774 __ x __ 0.062339
### teste 2: 0.056734 __ x __ 0.064401
### teste 3: 0.056453 __ x __ 0.063565
### teste 4: 0.029792 __ x __ 0.070024
### teste 5: 0.042123 __ x __ 0.070767

<br><br>










## Resultados

### Tempo sequencial minimo: 0.029792
### Tempo concorrente minimo threads(1): 0.034448
### Tempo concorrente minimo threads(2): 0.047493
### Tempo concorrente minimo threads(4): 0.062339
### Aceleração threads(1): 0,86
### Aceleração threads(2): 0,63
### Aceleração threads(4): 0,48







<br><br><br><br>



# dim 10^7

## dim 10^7 ______threads 1

### _________sequencial x concorrente

### teste 1: 3.006714 __ x __ 3.235872
### teste 2: 3.030440 __ x __ 3.287642
### teste 3: 3.060076 __ x __ 3.303947
### teste 4: 3.083126 __ x __ 3.277651
### teste 5: 2.996438 __ x __ 3.266745





<br><br>




## dim 10^7 ______threads 2

### _________sequencial x concorrente

### teste 1: 3.016299 __ x __ 2.510868
### teste 2: 3.046107 __ x __ 2.564362
### teste 3: 3.042323 __ x __ 2.545436
### teste 4: 3.037293 __ x __ 2.566042
### teste 5: 3.031332 __ x __ 2.540922





<br><br>

## dim 10^7 ______threads 4

### _________sequencial x concorrente

### teste 1: 2.995629 __ x __ 3.798990
### teste 2: 2.996294 __ x __ 3.736955
### teste 3: 3.011510 __ x __ 3.563776
### teste 4: 3.038860 __ x __ 3.618618
### teste 5: 3.035187 __ x __ 3.465064

<br><br>



## Resultados

### Tempo sequencial minimo: 2.996294
### Tempo concorrente minimo threads(1): 3.235872
### Tempo concorrente minimo threads(2): 2.510868
### Tempo concorrente minimo threads(4): 3.465064
### Aceleração threads(1): 0,93
### Aceleração threads(2): 1,19
### Aceleração threads(4): 0,86


<br><br><br><br>





















# Resultados

## dim 10^8 ______threads 1

### _________sequencial x concorrente

### teste 1: 30.212930 __ x __ 32.332198
### teste 2: 29.952629 __ x __ 32.300140
### teste 3: 29.946117 __ x __ 32.288643
### teste 4: 30.056708 __ x __ 32.233937
### teste 5: 30.251490 __ x __ 32.521242





<br><br>




## dim 10^8 ______threads 2

### _________sequencial x concorrente

### teste 1: 30.147790 __ x __ 25.018343
### teste 2: 30.353493 __ x __ 24.429833
### teste 3: 29.939332 __ x __ 24.840122
### teste 4: 29.929152 __ x __ 24.833894
### teste 5: 30.219226 __ x __ 24.988633





<br><br>

## dim 10^8 ______threads 4

### _________sequencial x concorrente

### teste 1: 30.190513 __ x __ 37.553803
### teste 2: 29.947552 __ x __ 37.455275
### teste 3: 29.947051 __ x __ 35.633333
### teste 4: 29.969191 __ x __ 37.366402
### teste 5: 29.929560 __ x __ 37.363667

<br><br>

<br><br>


## Resultados

### Tempo sequencial minimo: 29.929152
### Tempo concorrente minimo threads(1): 32.233937
### Tempo concorrente minimo threads(2): 24.429833
### Tempo concorrente minimo threads(4): 37.363667
### Aceleração threads(1): 0,93
### Aceleração threads(2): 1,23
### Aceleração threads(4): 0,8


<br><br><br><br>