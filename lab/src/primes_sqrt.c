#include"../libs/include.h"




int * vector;

int* entryVector;
float* resultVectorConc;
float* resultVectorSeq;
long long int dim;
int nthreads;

int ig;

pthread_mutex_t bastao;

int ehPrimo(long long int n);






void * task(void * arg){


    //pega a posicao do vetor a ser testada, garantindo seguranca com o lock
    pthread_mutex_lock(&bastao);
    int il = ig; ig++;
    pthread_mutex_unlock(&bastao);

    


    //realiza a raiz quadrada ou nao dependendo se o numero for primo ou noa
    while(il<dim){
        if (ehPrimo(entryVector[il]))
            resultVectorConc[il] = sqrt(entryVector[il]);
        else
            resultVectorConc[il] = entryVector[il];


        //atualiza a posicao a ser testada
        pthread_mutex_lock(&bastao);
        il = ig; ig++;
        pthread_mutex_unlock(&bastao);
    }


    pthread_exit(NULL);
}






int ehPrimo(long long int n) {
    if (n<=1) return 0;
    if (n==2) return 1;
    if (n%2==0) return 0;

    for (int i=3; i<sqrt(n)+1; i+=2)
        if(n%i==0) return 0;

    return 1;
}










int main(int argc, char ** argv){
    pthread_t *tid; //identificadores das threads no sistema


    double inicio, fim, delta, seqTime, concTime; //vars para capturar tempo


    //vars para gerar numero randomicos
    float ceiling = 1000000;
    float a = ((float)RAND_MAX)/(ceiling);






    //recebe dimensao e numero de threads
    if(argc < 3) {
       fprintf(stderr, "Digite: %s <dimensao do vetor> <numero threads>\n", argv[0]);
       return 1; 
    }

    dim = atoll(argv[1]);   
    nthreads = atoi(argv[2]);

    if(nthreads > dim) nthreads = dim;







    //inicializa vetor e outras variaveis
    entryVector = (int *)malloc(sizeof(int *)*dim);
    if(entryVector==NULL) {exit(1);}

    resultVectorConc = (float *)malloc(sizeof(float *)*dim);
    if(resultVectorConc==NULL) {exit(1);}

    resultVectorSeq = (float *)malloc(sizeof(float *)*dim);
    if(resultVectorSeq==NULL) {exit(1);}






    //preenche o vetor com numeros aleatorios
    srand ( time(NULL) );
    for(long long int i=0; i< dim; i++){
        a*=1.00000001;
        entryVector[i] = (int)(((float)rand()/a));
        resultVectorConc[i] = 0;
        resultVectorSeq[i] = 0;
    }





    //calcula concorrentemente
    GET_TIME(inicio);



    //aloca os identificadores
    tid = (pthread_t *) malloc(sizeof(pthread_t) * nthreads);
    if(tid==NULL) {exit(1);}

    //cria threads
    create_threads(tid, NULL, task, NULL, nthreads);

    //join threads
    join_threads(tid, NULL, nthreads);



    GET_TIME(fim);
    delta = fim - inicio;
    concTime = delta;






    //calcula sequencialmente

    GET_TIME(inicio);

    for(long long int i=0; i<dim; i++) {
        if (ehPrimo(entryVector[i]))
            resultVectorSeq[i] = sqrt(entryVector[i]);
        else
            resultVectorSeq[i] = (float)entryVector[i];
    }


    GET_TIME(fim);
    delta = fim - inicio;
    seqTime = delta;


    //erro forcado para teste
    //resultVectorSeq[dim-1]=0;




    //testa igualdade
    for(int i =0; i<dim; i++){
        if(resultVectorSeq[i] != resultVectorConc[i]){
            printf("Valores diferentes\n"); exit(2);
        }
    }

    


    //printa vetores para teste
    
    /*
    for(int i =0; i<dim; i++){
        printf("%5d ", entryVector[i]);
    }

    printf("\n");
    printf("\n");

    for(int i =0; i<dim; i++){
        printf("%5.0f ", resultVectorSeq[i]);
    }

    printf("\n");
    printf("\n");

    for(int i =0; i<dim; i++){
        printf("%5.0f ", resultVectorConc[i]);
    }

    printf("\n");
    */





    //printa tempo
    printf("Tempo:\n");
    printf("Sequencial  x  Concorrente\n");


    printf("%lf __ x __ %lf\n", seqTime, concTime);


    //free
    free(tid);
    free(entryVector);
    free(resultVectorConc);
    free(resultVectorSeq);

    return 0;
}